#!/usr/bin/env bash

# Created By: Jake@Linux
# Created On: Mon 29 Aug 2022 09:08:33 AM CDT
# Project: autostart for qtile

picom -cCGF &
feh --bg-scale ~/Pictures/piccas_img/pngtree-abstract-speed-background-vector-image.jpg &
~/./pipewire-pulse &
cbatticon  -i notification /sys/class/power_supply/BAT1 &
nm-applet &
fcitx5 -s 3 &
disable-suspend.sh &
